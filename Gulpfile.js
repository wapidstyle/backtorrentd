//
// Gulpfile for backtorrentd
//
const gulp = require("gulp");

gulp.task("babel", () => {
  return gulp.src("src/**/*.js")
    .pipe(require("gulp-babel")({
      "presets":[
        "env"
      ]
    }))
    .pipe(gulp.dest("out/"));
});
