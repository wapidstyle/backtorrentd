//
// Main daemon for backtorrentd.
//
// This code is under the BSD-3 license.
//
import {default as yaml} from "js-yaml";
import {default as tempFs} from "graceful-fs";
import {default as Promise} from "bluebird";
import {VM as VM} from "vm2";
import {transform as babel} from "babel-core";

const fs = Promise.promisifyAll(tempFs);

process.nextTick(function backtorrentd() {
  fs.readFileAsync(__dirname + "/../config.yml")
    .then(yaml.safeLoad)
    .then((data) => {
      if(data.version !== 0) {
        console.error("Config version is old (expected 0, got " + data.version + "!)");
        process.exit();
      }

      // Backup of require()
      const _require = require;
      // FIXME This code is a MESS, clean it up!
      let tmpfile = require("os").tmpdir() + "backtorrentd-"
                  + process.getuid() + "/" + Math.random() ** (512 * (Math.random() + 2));
      let vmOptions = {
        sandbox: {
          console,
          module: {
            children: [],
            exports: {},
            filename: tmpfile,
            id: tmpfile,
            loaded: true,
            parent: undefined
          },
          require(moduleName) {
            if (moduleName.startsWith("../")) {
              console.error("Error in vm2'd module: can't use ../* modules!");
              return undefined;
            } else if (moduleName.startsWith("./")) {
              let returnValue = null;

              data.blockedFiles.forEach((element) => {
                if (returnValue !== undefined && element === moduleName) {
                  returnValue = undefined;
                  console.error("Error in vm2'd module: module " + moduleName + " blocked");
                }
              });

              if (returnValue === null) {
                return vm.run(fs.readFileSync(moduleName)).module.exports;
              } else {
                return undefined;
              }
            } else {
              // Probably a library call
              let returnValue = null;

              data.blockedModules.forEach((element) => {
                if (returnValue === undefined && element === moduleName) {
                  returnValue = undefined;
                  console.error("Error in vm2'd module: module " + moduleName + " blocked");
                }
              });

              if (returnValue === null) {
                // TODO Run in sandbox
                return _require(moduleName);
              } else {
                return undefined;
              }
            }
          }
        }
      };
      vmOptions.sandbox.exports = vmOptions.sandbox.module.exports;
      let vm = new VM(vmOptions);

      if(data.sandboxish) {
        // Run the rest of the code in a vm2 container for security
        vm.run(babel(fs.readFileSync(`${__dirname}/intermediate.js`, "utf-8") + `\n_intermediate(${data});`).code);
      } else {
        require("./intermediate").default(data);
      }
    }).catch((err) => {
      console.error("An error has occurred.");
      throw err;
    });
});
