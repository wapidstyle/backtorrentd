# `backtorrentd`
A simple WebTorrent-based backend for IPFS and protocols like it.

## Principles
Go to [WebTorrent's Website](https://webtorrent.io/) and you'll be greeted with
a torrented video. On the left, there's a list of peers. That's the discovery
mechanism.

The user's browser would request a special torrent that (in theory) only
backtorrent servers have. They could then send a WebSocket request, asking
to run a script on the server. If the server's max-at-one-time limit is not
reached, then it will start a `vm2` instance and start running the script.

This is how it would go:
1. User downloads Torrent (via magnet link, most likely)
2. User gets one of their peers and sends a WebSocket request, saying hi
3. The server gets the request, starts a `vm2` instance and runs the script
with the parameters given by the user.
4. The server sends either the response or an OK/Failed state

## Usage
### Server
In **config.yml**:
```yaml
scripts:
  - hello:
      name: "Hello"
      location: "scripts/hello.js"
  - bye:
      name: "Goodbye"
      location: "scripts/bye.js"
```
In **scripts/hello.js**:
```js
logger.log("Hello, World!");
status = 0;
```
In **scripts/bye.js**:
```js
// Since this is Babel'd before running, we can use ES6!
import {default as _} from "lodash";
_.partition([0, 1, 2, 3], (n) => {
  return n % 2;
});
status = 0;
```
### Client
In **index.html** (or the name of your app's main page):
```html
<h1>My Application</h1>
<h3>Press the button to get an "Hello, World" on the server's console!</h3>
<button onclick="runapp()">GO!</button>
<!-- Scripts -->
<script src="path/to/webtorrent.min.js" />
<script src="path/to/backtorrent.min.js" />
<script>
  // Before anything else, set your UNIQUE_ID to something unique (even a random
  // SHA256 will do!)
  UNIQUE_ID = "9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08";

  function runapp() {
    // Send the request
    backtorrent.run("hello");
  }
</script>
```

## Pros/Cons
### Pros
* **100% decenteralized.** There's no behind-the-scenes stuff with IPFS - just
the HTML.
* **Completely free.** There's no cost to running BackTorrent (at most, a
Raspberry Pi.)

### Cons
* **A server** ***is*** **needed.** You can't just use a client; you need some
servers too.
* **If BitTorrent is blocked, so is this.** It's entirely possible to block
BitTorrent, in which case this will be blocked.
